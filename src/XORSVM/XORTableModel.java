/*            DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
 Version 2, December 2004

 Copyright (C) 2015 Nathan Oliveira <oliveiranathan at gmail.com>

 Everyone is permitted to copy and distribute verbatim or modified
 copies of this code, and changing it is allowed as long  as the
 name is changed.

 DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
 TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION

 0. You just DO WHAT THE FUCK YOU WANT TO.
 */
package XORSVM;

import javax.swing.table.AbstractTableModel;

/**
 *
 * @author Nathan Oliveira <oliveiranathan at gmail.com>
 */
public class XORTableModel extends AbstractTableModel {

    private static final long serialVersionUID = 1L;

    private int qttyVars;
    private short[][] data;

    public XORTableModel() {
	qttyVars = 3;
	regenerateData();
    }

    @Override
    public int getRowCount() {
	if (qttyVars == 0) {
	    return 0;
	}
	return (int) Math.pow(2, qttyVars);
    }

    @Override
    public int getColumnCount() {
	if (qttyVars == 0) {
	    return 0;
	}
	return qttyVars + 1;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
	return data[rowIndex][columnIndex];
    }

    @Override
    public String getColumnName(int column) {
	if (column == getColumnCount() - 1) {
	    return "d_i";
	}
	return "x_" + column;
    }

    void setVarsNumber(int number) {
	qttyVars = number;
	regenerateData();
    }

    private void regenerateData() {
	data = new short[getRowCount()][getColumnCount()];
	for (int col = qttyVars - 1; col >= 0; col--) {
	    int qttyRepetitions = (int) Math.pow(2, qttyVars - col - 1);
	    short value = 1;
	    for (int row = 0; row < getRowCount(); row += qttyRepetitions) {
		if (value == 1) {
		    value = -1;
		} else {
		    value = 1;
		}
		for (int i = 0; i < qttyRepetitions; i++) {
		    data[row + i][col] = value;
		}
	    }
	}
	for (int row = 0; row < getRowCount(); row++) {
	    int count = 0;
	    for (int col = 0; col < qttyVars; col++) {
		if (data[row][col] == 1) {
		    count++;
		}
	    }
	    if (count % 2 == 1) {
		data[row][qttyVars] = 1;
	    } else {
		data[row][qttyVars] = -1;
	    }
	}
	fireTableStructureChanged();
    }
}
