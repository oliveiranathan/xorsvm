/*            DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
 Version 2, December 2004

 Copyright (C) 2015 Nathan Oliveira <oliveiranathan at gmail.com>

 Everyone is permitted to copy and distribute verbatim or modified
 copies of this code, and changing it is allowed as long  as the
 name is changed.

 DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
 TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION

 0. You just DO WHAT THE FUCK YOU WANT TO.
 */
package XORSVM;

import Polinomials.Polinomial;
import Polinomials.Term;
import Polinomials.Token;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import javax.swing.JOptionPane;
import org.apache.commons.math3.fraction.Fraction;

/**
 *
 * @author Nathan Oliveira <oliveiranathan at gmail.com>
 */
public class XORSVMSolutionCreator {

    private final int numberOfXORInputs;
    private final File outputFile;
    private final XORTableModel inputVectors;

    private transient StringBuilder outData;
    private transient Polinomial kernel;
    private transient Polinomial lagrange;
    private transient Polinomial[] lagrPartials;

    public XORSVMSolutionCreator(int numberofXORInputs, File outFile, XORTableModel input) {
	this.numberOfXORInputs = numberofXORInputs;
	outputFile = outFile;
	inputVectors = input;
    }

    public void computeSolution() {
	createDataStructures();
	populateDataStructures();
	flushToFile();
    }

    private void createDataStructures() {
	outData = new StringBuilder(512);
    }

    private void flushToFile() {
	if (outputFile == null) {
	    return;
	}
	try (BufferedOutputStream os = new BufferedOutputStream(new FileOutputStream(outputFile))) {
	    String outStr = outData.toString();
	    os.write(outStr.getBytes(), 0, outStr.length());
	    os.flush();
	} catch (IOException ex) {
	    java.util.logging.Logger.getLogger(XORSVMSolutionCreator.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
	}
    }

    private void populateDataStructures() {
	int twoAtNumber = (int) Math.pow(2, numberOfXORInputs);
	outData.append("The XOR Problem is defined by the following table:\n\n");
	outData.append("\\begin{tabular}{lc}\n\\hline\nInput vector $\\textbf{x}$ & Desired response $d$ \\\\\n\\hline\n");
	outData.append("");
	for (int row = 0; row < twoAtNumber; row++) {
	    String input = "$(";
	    short s; // hides the cast too strong warning.
	    for (int col = 0; col < numberOfXORInputs; col++) {
		s = (short) inputVectors.getValueAt(row, col);
		input += (s > 0 ? "+" : "") + s + ", ";
	    }
	    input = input.substring(0, input.length() - 2);
	    input += ")$";
	    s = (short) inputVectors.getValueAt(row, numberOfXORInputs);
	    outData.append(input).append(" & $").append(s > 0 ? "+" : "").append(s).append("$ \\\\\n");
	}
	outData.append("\\hline\n\\end{tabular}\n\n");
	outData.append("First, we define a polinomial kernel as:\n");
	outData.append("\\[k(\\textbf{x}, \\textbf{x}_i) = (1 + \\textbf{x}^T\\textbf{x}_i)^").append(numberOfXORInputs).append(" \\]\n");
	outData.append("With $\\textbf{x} = [");
	String[] varsNames = new String[numberOfXORInputs * 2];
	for (int x = 1; x <= numberOfXORInputs; x++) {
	    varsNames[x - 1] = "x_" + x;
	    outData.append("x_").append(x).append(",");
	}
	outData.deleteCharAt(outData.length() - 1);
	outData.append("]^T$ and $\\textbf{x}_i = [");
	for (int x = 1; x <= numberOfXORInputs; x++) {
	    varsNames[x - 1 + numberOfXORInputs] = "x_{i" + x + "}";
	    outData.append("x_{i").append(x).append("},");
	}
	outData.deleteCharAt(outData.length() - 1);
	outData.append("]^T$,\n then, we may express the kernel $k(\\textbf{x},\\textbf{x}_i)$ in terms of \\textit{monomials} of various orders:\n");
	outData.append("\\[k(\\textbf{x}, \\textbf{x}_i) = ");
	kernel = new Polinomial().addTerm(new Term().multByElement(new Token(1)));
	for (int x = 1; x <= numberOfXORInputs; x++) {
	    kernel.addTerm(new Term().multByElement(new Token(varsNames[x - 1])).multByElement(new Token(varsNames[x - 1 + numberOfXORInputs])));
	}
	kernel.pow(numberOfXORInputs);
	outData.append(kernel.toLaTeX()).append(". \\]\nThe image of the input vector $\\textbf{x}$ is therefore deduced as\n");
	outData.append("\\[ \\phi(\\textbf{x})=[");
	outData.append(kernel.toPhi());
	outData.append("]^T \\]\nSimilarly,\n");
	outData.append("\\[ \\phi(\\textbf{x}_i)=[");
	outData.append(kernel.toPhi_i());
	outData.append("]^T, i = ");
	switch (twoAtNumber) {
	    case 0:
		break;
	    case 1:
		outData.append("1");
		break;
	    case 2:
		outData.append("1, 2");
		break;
	    case 4:
		outData.append("1, 2, 3, 4");
		break;
	    default:
		outData.append("1, 2, \\dots, N");
	}
	outData.append(". \\]\n");
	outData.append("Using $\\textbf{K}=\\{k(\\textbf{x}_i, \\textbf{x}_j)\\}^N_{i,j=1}$, we obtain the Gram\n");
	outData.append("\\[K = \\begin{bmatrix}\n");
	double[][] gramValues = new double[twoAtNumber][twoAtNumber];
	for (int j = 0; j < twoAtNumber; j++) {
	    for (int i = 0; i < twoAtNumber; i++) {
		double values[] = new double[varsNames.length];
		for (int k = 0; k < numberOfXORInputs; k++) {
		    short s = (short) inputVectors.getValueAt(i, k);
		    values[k] = s;
		    s = (short) inputVectors.getValueAt(j, k);
		    values[k + numberOfXORInputs] = s;
		}
		outData.append(kernel.calc(varsNames, values));
		gramValues[j][i] = kernel.calc(values, varsNames);
		if (i < twoAtNumber - 1) {
		    outData.append(" & ");
		}
	    }
	    outData.append("\\\\\n");
	}
	outData.append("\\end{bmatrix} \\]\n\n");
	outData.append("Using $Q(\\alpha)=\\sum_{i=1}^N \\alpha_i - \\frac{1}{2} \\sum_{i=1}^N \\sum_{j=1}^N \\alpha_i \\alpha_j d_i d_j");
	outData.append("k(\\textbf{x}_i,\\textbf{x}_j)$ we determine the objective function for the dual form of optimization as follows:\n");
	lagrange = new Polinomial();
	String[] lagrVarsNames = new String[twoAtNumber];
	for (int i = 1; i <= twoAtNumber; i++) {
	    lagrVarsNames[i - 1] = "\\alpha_" + i;
	    lagrange.addTerm(new Term().multByElement(new Token(lagrVarsNames[i - 1])));
	}
	for (int i = 1; i <= twoAtNumber; i++) {
	    for (int j = 1; j <= twoAtNumber; j++) {
		short s = (short) inputVectors.getValueAt(i - 1, numberOfXORInputs);
		s *= (short) inputVectors.getValueAt(j - 1, numberOfXORInputs);
		lagrange.addTerm(new Term().multByElement(new Token(s > 0 ? -0.5 : 0.5))
			.multByElement(new Token(lagrVarsNames[i - 1])).multByElement(new Token(lagrVarsNames[j - 1]))
			.multByElement(new Token(gramValues[i - 1][j - 1])));
	    }
	}
	outData.append("\\[ Q(\\alpha)=").append(lagrange.toLaTeX()).append(". \\]\n");
	lagrPartials = new Polinomial[twoAtNumber];
	for (int i = 1; i <= twoAtNumber; i++) {
	    lagrPartials[i - 1] = lagrange.derive(new Token(lagrVarsNames[i - 1]));
	}
	outData.append("Optimizing $Q(\\alpha)$ with respect to the ").append(twoAtNumber).append(" Lagrange multipliers yields the following system of equations:\n");
	for (Polinomial lagrPartial : lagrPartials) {
	    outData.append("\\[").append(lagrPartial.toLaTeX()).append(" = 0 \\]\n");
	}
	double[] lagrMultValues = Polinomial.solveLagrangeSystem(lagrPartials, lagrVarsNames);
	outData.append("Hence, the optimum values of the Lagrange multipliers are:");
	Fraction[] fracs = new Fraction[lagrMultValues.length];
	int firstNum = 0, firstDen = 0;
	boolean equals = true;
	for (int i = 0; i < lagrMultValues.length; i++) {
	    fracs[i] = new Fraction(lagrMultValues[i]);
	    if (i == 0) {
		firstNum = fracs[i].getNumerator();
		firstDen = fracs[i].getDenominator();
	    } else if (firstNum != fracs[i].getNumerator()
		    || firstDen != fracs[i].getDenominator()) {
		equals = false;
	    }
	}
	if (equals) {
	    outData.append("\\[");
	    for (int i = 1; i <= twoAtNumber; i++) {
		outData.append("\\alpha_{o,").append(i).append("} = ");
	    }
	    if (firstDen == 1) {
		outData.append(fracs[0].getNumerator());
	    } else {
		outData.append("\\frac{").append(fracs[0].getNumerator()).append("}{").append(fracs[0].getDenominator()).append("}");
	    }
	    outData.append("\\]\n");
	    outData.append("This result indicates that all ").append(twoAtNumber).append(" input vectors $\\{\\textbf{x}_i\\}_{i=1}^").append(twoAtNumber);
	    outData.append("$ are support vectors.\n");
	} else {
	    // TODO later if needed for the higher orders.
	}
	outData.append("The optimum value of $Q(\\alpha)$ is");
	Fraction f = new Fraction(lagrange.calc(lagrMultValues, lagrVarsNames));
	outData.append("\\[Q_o(\\alpha) = ");
	if (firstDen == 1) {
	    outData.append(f.getNumerator());
	} else {
	    outData.append("\\frac{").append(f.getNumerator()).append("}{").append(f.getDenominator()).append("}");
	}
	outData.append("\\]\n");
	outData.append("Correspondingly, we may write\n");
	outData.append("\\[\\frac{1}{2}||\\textbf{w}_o||^2 = ");
	if (firstDen == 1) {
	    outData.append(f.getNumerator());
	} else {
	    outData.append("\\frac{").append(f.getNumerator()).append("}{").append(f.getDenominator()).append("}");
	}
	outData.append("\\]\nor\n");
	f = f.multiply(2);
	outData.append("\\[||\\textbf{w}_o|| = ");
	if (firstDen == 1) {
	    outData.append("\\sqrt").append(f.getNumerator());
	} else {
	    outData.append("\\frac{").append(f.getNumerator() == 1 ? "" : "\\sqrt").append(f.getNumerator())
		    .append("}{").append("\\sqrt").append(f.getDenominator()).append("}");
	}
	outData.append("\\]\n");
	outData.append("Then, we can find the optimum weight vector $\\textbf{w}_o = \\sum_{i=1}^N \\alpha_i d_i \\phi(\\textbf{x}_i)$\n");
	outData.append("\\[ \\textbf{w}_o = ");
	for (int i = 1; i <= twoAtNumber; i++) {
	    short s = (short) inputVectors.getValueAt(i - 1, numberOfXORInputs);
	    if (s < 1) {
		outData.append("-");
	    } else {
		if (i > 1) {
		    outData.append("+");
		}
	    }
	    if (fracs[i - 1].getDenominator() == 1) {
		outData.append(f.getNumerator());
	    } else {
		outData.append("\\frac{").append(fracs[i - 1].getNumerator()).append("}{").append(fracs[i - 1].getDenominator()).append("}");
	    }
	    outData.append("\\varphi(\\textbf{x}_").append(i).append(")");
	}
	outData.append("\\]\n");
	double[][] vectorsValues = new double[twoAtNumber][kernel.getNumberOfTerms()];
	kernel.calcPhi(lagrMultValues, inputVectors, vectorsValues);
	outData.append("\\[\n\\renewcommand\\arraystretch{1.5}\n=\\begin{bmatrix}\n");
	for (int i = 0; i < twoAtNumber; i++) {
	    if (i > 0) {
		outData.append(" &\n+");
	    }
	    outData.append("\\begin{bmatrix}");
	    for (int j = 0; j < vectorsValues[i].length; j++) {
		if (j > 0) {
		    outData.append(" \\\\\n");
		}
		Fraction ff = new Fraction(vectorsValues[i][j]);
		if (ff.getDenominator() == 1) {
		    outData.append(ff.getNumerator());
		} else {
		    if (vectorsValues[i][j] < 0) {
			outData.append("-");
		    }
		    outData.append("\\frac{").append(vectorsValues[i][j] < 0 ? -ff.getNumerator() : ff.getNumerator()).append("}{").append(ff.getDenominator()).append("}");
		}
	    }
	    outData.append("\\end{bmatrix}");
	}
	outData.append("\\end{bmatrix} \\]\n");
	outData.append("\\[\n\\renewcommand\\arraystretch{1.5}\n=\\begin{bmatrix}\n");
	boolean isBiasZero = false;
	double[] wOptimal = new double[vectorsValues[0].length];
	for (int j = 0; j < vectorsValues[0].length; j++) {
	    double value = 0;
	    for (int i = 0; i < twoAtNumber; i++) {
		value += vectorsValues[i][j];
	    }
	    if (j == 0 && value == 0) {
		isBiasZero = true;
	    }
	    wOptimal[j] = value;
	    Fraction ff = new Fraction(value);
	    if (j > 0) {
		outData.append(" \\\\\n");
	    }
	    if (ff.getDenominator() == 1) {
		outData.append(ff.getNumerator());
	    } else {
		if (value < 0) {
		    outData.append("-");
		}
		outData.append("\\frac{").append(value < 0 ? -ff.getNumerator() : ff.getNumerator()).append("}{").append(ff.getDenominator()).append("}");
	    }
	}
	outData.append("\\end{bmatrix} \\]\n");
	if (isBiasZero) {
	    outData.append("The first element of $\\textbf{w}_o$ indicates that the bias $b$ is zero.\n");
	}
	outData.append("The optimal hyperplane is defined by\n");
	outData.append("\\[\\textbf{w}_o^T\\phi(\\textbf{x})=0\\]\n");
	outData.append("Expanding the inner product $\\textbf{w}_o^T\\phi(\\textbf{x})$ yields:\n");
	outData.append("\\[\\begin{bmatrix}\n");
	for (int j = 0; j < wOptimal.length; j++) {
	    if (j > 0) {
		outData.append(", ");
	    }
	    Fraction ff = new Fraction(wOptimal[j]);
	    if (ff.getDenominator() == 1) {
		outData.append(ff.getNumerator());
	    } else {
		if (wOptimal[j] < 0) {
		    outData.append("-");
		}
		outData.append("\\frac{").append(wOptimal[j] < 0 ? -ff.getNumerator() : ff.getNumerator()).append("}{").append(ff.getDenominator()).append("}");
	    }
	}
	outData.append("\\end{bmatrix}\n");
	outData.append("\\renewcommand\\arraystretch{1.5}\n\\begin{bmatrix}");
	outData.append(kernel.toPhiVector());
	outData.append("\\end{bmatrix} = 0 \\]\n");
	outData.append("which reduces to\n");
	Polinomial result = kernel.reducePhiTo(wOptimal);
	outData.append("\\[").append(result.toLaTeX()).append(" = 0. \\]");
	JOptionPane.showMessageDialog(null, "The LaTeX file has been generated.");
    }
}
