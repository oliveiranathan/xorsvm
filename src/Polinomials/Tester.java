/*            DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
 Version 2, December 2004

 Copyright (C) 2015 Nathan Oliveira <oliveiranathan at gmail.com>

 Everyone is permitted to copy and distribute verbatim or modified
 copies of this code, and changing it is allowed as long  as the
 name is changed.

 DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
 TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION

 0. You just DO WHAT THE FUCK YOU WANT TO.
 */
package Polinomials;

/**
 *
 * @author Nathan Oliveira <oliveiranathan at gmail.com>
 */
public class Tester {

    public static void main(String[] args) {
//	testToken(); // TODO redo
//	testTerm(); // TODO redo
//	testPolinomial(); // TODO redo
	testFractionSplitter();
    }

    private static void testToken() {
	Token t1 = new Token("x_i");
	Token t2 = new Token("x_1", 5);
	Token t3 = new Token(666);
	Token t4 = new Token(24601, 2);
	assert "''x_i'^1.0'".equals(t1.toString());
	assert "''x_1'^5.0'".equals(t2.toString());
	assert "'666.0^1.0'".equals(t3.toString());
	assert "'24601.0^2.0'".equals(t4.toString());
	t3.incPower(665);
	assert "'666.0^666.0'".equals(t3.toString());
	Token t5 = t1.deepCopy();
	t1.incPower();
	assert "''x_i'^2.0'".equals(t1.toString());
	assert "''x_i'^1.0'".equals(t5.toString());
	assert t1 != t5;
	assert t1.isSame(t5);
	assert t5.isSame(t1);
	t5.incPower();
	assert t1.equals(t5);
	assert t5.equals(t1);
	Token t6 = t4.deepCopy();
	assert t4 != t6;
	assert t4.equals(t6);
	System.out.println("Tokens are OK.");
    }

    private static void testTerm() {
	Token tk1 = new Token("x_i", 2);
	Token tk2 = new Token("x", 2);
	Token tk3 = new Token(2);
	Term t1 = new Term();
	t1.multByElement(tk1);
	t1.multByElement(tk2);
	t1.multByElement(tk3);
	assert "\"'2.0^1.0'''x'^2.0'''x_i'^2.0'\"".equals(t1.toString());
	Term t2 = new Term();
	t2.multByElement(tk3);
	t2.multByElement(tk2);
	t2.multByElement(tk1);
	assert "\"'2.0^1.0'''x'^2.0'''x_i'^2.0'\"".equals(t2.toString());
	assert t1 != t2;
	assert t1.toString().equals(t2.toString());
	Term t3 = t1.deepCopy();
	assert t3 != t1;
	assert t1.haveSameVars(t3);
	assert t1.isSame(t3);
	assert "\"'2.0^1.0'''x'^2.0'''x_i'^2.0'\"".equals(t3.toString());
	t1.add(t3);
	assert "\"'4.0^1.0'''x'^2.0'''x_i'^2.0'\"".equals(t1.toString());
	t1.multByElement(tk1);
	assert "\"'4.0^1.0'''x'^2.0'''x_i'^4.0'\"".equals(t1.toString());
	t1.multByTerm(t3);
	assert "\"'8.0^1.0'''x'^4.0'''x_i'^6.0'\"".equals(t1.toString());
	Term t4 = new Term();
	t4.multByElement(new Token(5, 2));
	t4.multByElement(new Token(3, 3));
	t4.multByElement(new Token(2, 1));
	t4.multByElement(new Token("a", 4));
	t4.multByElement(new Token("x", 5));
	assert "\"'2.0^1.0''3.0^3.0''5.0^2.0'''a'^4.0'''x'^5.0'\"".equals(t4.toString());
	t4.multByTerm(t1);
	assert "\"'3.0^3.0''5.0^2.0''16.0^1.0'''a'^4.0'''x'^9.0'''x_i'^6.0'\"".equals(t4.toString());
	System.out.println("Terms are OK.");
    }

    private static void testPolinomial() {
	Polinomial p1 = new Polinomial();
	p1.addTerm(new Term().multByElement(new Token(5)).multByElement(new Token("x_i", 2)).multByElement(new Token("x", 2)));
	assert "[\"'5.0^1.0'''x'^2.0'''x_i'^2.0'\"]".equals(p1.toString());
	p1.addTerm(new Term().multByElement(new Token(5)).multByElement(new Token("x_i", 2)).multByElement(new Token("x", 2)));
	assert "[\"'10.0^1.0'''x'^2.0'''x_i'^2.0'\"]".equals(p1.toString());
	p1.addTerm(new Term().multByElement(new Token("a")));
	assert "[\"'10.0^1.0'''x'^2.0'''x_i'^2.0'\"\"''a'^1.0'\"]".equals(p1.toString());
	Polinomial p2 = new Polinomial();
	p2.addTerm(new Term().multByElement(new Token("a")));
	p2.addTerm(new Term().multByElement(new Token("b")));
	assert "[\"''a'^1.0'\"\"''b'^1.0'\"]".equals(p2.toString());
	Polinomial p3 = p1.deepCopy();
	assert p1 != p3;
	assert "[\"'10.0^1.0'''x'^2.0'''x_i'^2.0'\"\"''a'^1.0'\"]".equals(p1.toString());
	assert "[\"'10.0^1.0'''x'^2.0'''x_i'^2.0'\"\"''a'^1.0'\"]".equals(p3.toString());
	p3.addPolinomial(p2);
	assert "[\"'10.0^1.0'''x'^2.0'''x_i'^2.0'\"\"'2.0^1.0'''a'^1.0'\"\"''b'^1.0'\"]".equals(p3.toString());
	p3 = p1.deepCopy();
	p3.multByTerm(new Term().multByElement(new Token(2, 2)).multByElement(new Token("a")));
	assert "[\"'2.0^2.0''10.0^1.0'''a'^1.0'''x'^2.0'''x_i'^2.0'\"\"'2.0^2.0'''a'^2.0'\"]".equals(p3.toString());
	assert "[\"'10.0^1.0'''x'^2.0'''x_i'^2.0'\"\"''a'^1.0'\"]".equals(p1.toString());
	p3 = p1.deepCopy();
	p3.multByPolinomial(p1);
	assert "[\"'10.0^1.0'''x'^2.0'''x_i'^2.0'\"\"''a'^1.0'\"]".equals(p1.toString());
	assert "[\"'10.0^2.0'''x'^4.0'''x_i'^4.0'\"\"'20.0^1.0'''a'^1.0'''x'^2.0'''x_i'^2.0'\"\"''a'^2.0'\"]".equals(p3.toString());
	Polinomial ab = new Polinomial().addTerm(new Term().multByElement(new Token("a"))).addTerm(new Term().multByElement(new Token("b")));
	Polinomial abC = ab.deepCopy();
	abC.multByPolinomial(ab);
	assert "[\"''a'^2.0'\"\"'2.0^1.0'''a'^1.0'''b'^1.0'\"\"''b'^2.0'\"]".equals(abC.toString());
	abC = ab.deepCopy();
	abC.pow(2);
	assert "[\"''a'^2.0'\"\"'2.0^1.0'''a'^1.0'''b'^1.0'\"\"''b'^2.0'\"]".equals(abC.toString());
	abC = ab.deepCopy();
	abC.pow(3);
	assert "[\"''a'^3.0'\"\"'3.0^1.0'''a'^2.0'''b'^1.0'\"\"'3.0^1.0'''a'^1.0'''b'^2.0'\"\"''b'^3.0'\"]".equals(abC.toString());
	System.out.println("Polinomials are OK.");
    }

    public static String toMixedFraction(double x) {
	int maxFrac = 2048;
	int w = (int) x,
		n = (int) (x * maxFrac) % maxFrac,
		a = n & -n;
	return (w == 0 && n != 0 ? "" : w) + (n == 0 ? "" : " " + n / a + "/" + maxFrac / a);
    }

    private static void testFractionSplitter() {
	int maxFrac = 2048;
	for (int i = 0; i <= maxFrac; i++) {
	    double value = (double) i / maxFrac;
	    System.out.println(value + "\t\t\t" + toMixedFraction(value));
	}
    }
}
