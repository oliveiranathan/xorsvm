/*            DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
 Version 2, December 2004

 Copyright (C) 2015 Nathan Oliveira <oliveiranathan at gmail.com>

 Everyone is permitted to copy and distribute verbatim or modified
 copies of this code, and changing it is allowed as long  as the
 name is changed.

 DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
 TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION

 0. You just DO WHAT THE FUCK YOU WANT TO.
 */
package Polinomials;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 *
 * @author Nathan Oliveira <oliveiranathan at gmail.com>
 */
public class Term {

    private final List<Token> tokens = new ArrayList<>(2);

    @SuppressWarnings("AccessingNonPublicFieldOfAnotherObject")
    public Term multByTerm(Term term) {
	term.tokens.stream().forEach((tk) -> multByElement(tk));
	return this;
    }

    public Term multByElement(Token tk) {
	if (tokens.isEmpty()) {
	    tokens.add(tk.deepCopy());
	    return this;
	}
	if (tokens.size() == 1 && tokens.get(0).getType() == Token.Type.NUMBER && tokens.get(0).getNumber() == 1) {
	    tokens.set(0, tk.deepCopy());
	    return this;
	}
	boolean done = false;
	for (Token t : tokens) {
	    if (t.isSame(tk)) {
		t.incPower(tk.getPower());
		done = true;
		break;
	    }
	}
	if (!done) {
	    insert(tk.deepCopy());
	    compressNumbers();
	}
	return this;
    }

    @SuppressWarnings("AccessingNonPublicFieldOfAnotherObject")
    public boolean isSame(Term term) {
	if (term == null) {
	    return false;
	}
	if (tokens.size() != term.tokens.size()) {
	    return false;
	}
	for (int i = 0; i < tokens.size(); i++) {
	    if (!tokens.get(i).equals(term.tokens.get(i))) {
		return false;
	    }
	}
	return true;
    }

    @SuppressWarnings("AccessingNonPublicFieldOfAnotherObject")
    public boolean haveSameVars(Term term) {
	if (term == null) {
	    return false;
	}
	int idxThis = 0, idxOth = 0;
	while (idxThis < tokens.size() && tokens.get(idxThis).getType() == Token.Type.NUMBER) {
	    idxThis++;
	}
	while (idxOth < term.tokens.size() && term.tokens.get(idxOth).getType() == Token.Type.NUMBER) {
	    idxOth++;
	}
	if (tokens.size() - idxThis != term.tokens.size() - idxOth) {
	    return false;
	}
	for (; idxThis < tokens.size(); idxThis++, idxOth++) {
	    if (!tokens.get(idxThis).equals(term.tokens.get(idxOth))) {
		return false;
	    }
	}
	return true;
    }

    public Term deepCopy() {
	Term t = new Term();
	tokens.stream().forEach((tk) -> t.multByElement(tk));
	return t;
    }

    @SuppressWarnings("AccessingNonPublicFieldOfAnotherObject")
    public Term add(Term term) throws ArithmeticException {
	if (!haveSameVars(term)) {
	    throw new ArithmeticException("Cannot add terms that have different variables. Create a Polinomial instead.");
	}
	double thisNumber = tokens.stream().filter((tk) -> tk.getType() == Token.Type.NUMBER).map((tk) -> Math.pow(tk.getNumber(), tk.getPower())).reduce(0.0, Double::sum);
	if (thisNumber == 0) {
	    thisNumber = 1;
	}
	double othNumber = term.tokens.stream().filter((tk) -> tk.getType() == Token.Type.NUMBER).map((tk) -> Math.pow(tk.getNumber(), tk.getPower())).reduce(0.0, Double::sum);
	if (othNumber == 0) {
	    othNumber = 1;
	}
	tokens.removeIf((tk) -> tk.getType() == Token.Type.NUMBER);
	insert(new Token(thisNumber + othNumber));
	return this;
    }

    private void insert(Token tk) {
	if (tk.getType() == Token.Type.NUMBER) {
	    int insPosition = 0;
	    while (insPosition < tokens.size() && tokens.get(insPosition).getType() == Token.Type.NUMBER
		    && tokens.get(insPosition).getNumber() < tk.getNumber()) {
		insPosition++;
	    }
	    tokens.add(insPosition, tk);
	} else {
	    int insPosition = 0;
	    while (insPosition < tokens.size() && tokens.get(insPosition).getType() == Token.Type.NUMBER) {
		insPosition++;
	    }
	    while (insPosition < tokens.size() && tokens.get(insPosition).getToken().compareTo(tk.getToken()) < 0) {
		insPosition++;
	    }
	    tokens.add(insPosition, tk);
	}
    }

    @Override
    public String toString() {
	return "{" + tokens.stream().map((tk) -> tk.toString()).reduce("", String::concat) + "}";
    }

    public String toLaTeX() {
	return "{" + tokens.stream().map((tk) -> tk.toLaTeX()).reduce("", String::concat) + "}";
    }

    private void compressNumbers() {
	List<Token> numbers = new ArrayList<>(tokens);
	numbers.removeIf((tk) -> tk.getType() != Token.Type.NUMBER);
	tokens.removeIf((tk) -> tk.getType() == Token.Type.NUMBER);
	while (!numbers.isEmpty()) {
	    Token number = numbers.remove(0);
	    for (Token tk : numbers) {
		if (tk.getPower() == number.getPower()) {
		    tk.multNumber(number.getNumber());
		    number = null;
		    break;
		}
	    }
	    if (number != null) {
		insert(number);
	    }
	}
    }

    String toPhi() {
	return "{" + tokens.stream().map((tk) -> tk.toPhi()).reduce("", String::concat) + "}";
    }

    String toPhi_i() {
	return "{" + tokens.stream().map((tk) -> tk.toPhi_i()).reduce("", String::concat) + "}";
    }

    double calc(StringBuilder vars, Map<String, Double> varsValues) {
	if (vars == null || !vars.toString().isEmpty()) {
	    throw new IllegalArgumentException("\"vars\" should be not null and empty.");
	}
	double result = 1;
	for (Token t : tokens) {
	    switch (t.getType()) {
		case NUMBER:
		    result *= Math.pow(t.getNumber(), t.getPower());
		    break;
		case VARIABLE:
		    if (varsValues.containsKey(t.getToken())) {
			result *= Math.pow(varsValues.get(t.getToken()), t.getPower());
		    } else {
			vars.append(t.toLaTeX());
		    }
		    break;
	    }
	}
	if (result == 0) {
	    vars.delete(0, vars.length());
	    return 0;
	}
	if (!vars.toString().isEmpty()) {
	    if (result != 1) {
		vars.insert(0, (result == Math.round(result) ? Math.round(result) : result) + " ");
	    }
	}
	return result;
    }

    double calc(Map<String, Double> varsValues) throws ArithmeticException {
	double result = 1;
	for (Token t : tokens) {
	    switch (t.getType()) {
		case NUMBER:
		    result *= Math.pow(t.getNumber(), t.getPower());
		    break;
		case VARIABLE:
		    if (varsValues.containsKey(t.getToken())) {
			result *= Math.pow(varsValues.get(t.getToken()), t.getPower());
		    } else {
			throw new ArithmeticException("Token \"" + t.getToken() + "\" does not have an associated value.");
		    }
		    break;
	    }
	}
	return result;
    }

    double getNumber() {
	double result = 1;
	result = tokens.stream().filter((t) -> (t.getType() == Token.Type.NUMBER))
		.map((t) -> Math.pow(t.getNumber(), t.getPower())).reduce(result, (accumulator, _item) -> accumulator * _item);
	return result;
    }

    boolean hasVar(Token token) {
	return tokens.stream().anyMatch((t) -> (t.isSame(token)));
    }

    Term derivate(Token token) {
	if (!hasVar(token)) {
	    return new Term().multByElement(new Token(0));
	}
	Term derivate = new Term();
	tokens.stream().forEach((t) -> {
	    if (t.isSame(token)) {
		if (t.getPower() != 1 || tokens.size() == 1) {
		    derivate.multByElement(new Token(t.getPower()));
		}
		if (t.getPower() - 1 != 0) {
		    derivate.multByElement(new Token(t.getToken(), t.getPower() - 1));
		}
	    } else {
		derivate.multByElement(t);
	    }
	});
	return derivate;
    }

    double calcPhi(short[] vals) {
	double result = 1;
	for (Token t : tokens) {
	    if (t.isPhi()) {
		switch (t.getType()) {
		    case NUMBER:
			result *= Math.sqrt(Math.pow(t.getNumber(), t.getPower()));
			break;
		    case VARIABLE:
			String var = t.getToken().substring(2);
			int i = Integer.parseInt(var);
			result *= Math.pow(vals[i - 1], t.getPower());
			break;
		}
	    }
	}
	return result;
    }

    Term getPhi() {
	Term retVal = new Term();
	for (Token tk : tokens) {
	    if (tk.isPhi()) {
		retVal.multByElement(tk);
	    }
	}
	return retVal;
    }
}
