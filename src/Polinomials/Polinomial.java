/*            DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
 Version 2, December 2004

 Copyright (C) 2015 Nathan Oliveira <oliveiranathan at gmail.com>

 Everyone is permitted to copy and distribute verbatim or modified
 copies of this code, and changing it is allowed as long  as the
 name is changed.

 DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
 TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION

 0. You just DO WHAT THE FUCK YOU WANT TO.
 */
package Polinomials;

import XORSVM.XORTableModel;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author Nathan Oliveira <oliveiranathan at gmail.com>
 */
public class Polinomial {

    public static double[] solveLagrangeSystem(Polinomial[] lagrPartials, String[] lagrVarsNames) {
	if (lagrPartials.length != lagrVarsNames.length) {
	    throw new IllegalArgumentException("Both vectors should have the same length.");
	} // should be checked if it has the Lagrenge partials form, but whatever
	Map<Token, Polinomial> polinomialMap = new HashMap<>(lagrVarsNames.length);
	Map<Token, Double> resultMap = new HashMap<>(lagrVarsNames.length);
	for (int i = 0; i < lagrVarsNames.length; i++) {
	    Token tk = new Token(lagrVarsNames[i]);
	    Polinomial p = lagrPartials[i].deepCopy();
	    for (int j = 0; j < i; j++) {
		Token lastTk = new Token(lagrVarsNames[j]);
		double value = p.getNumberFor(lastTk);
		p.removeTermWith(lastTk);
		Polinomial fuckThis = polinomialMap.get(lastTk).deepCopy();
		Term pqp = new Term().multByElement(new Token(value));
		fuckThis = fuckThis.multByTerm(pqp);
		p.addPolinomial(fuckThis);
	    }
	    p = p.isolate(tk); // Solve equation for var...
	    polinomialMap.put(tk, p); // Save for later.
	}
	for (int i = lagrVarsNames.length - 1; i >= 0; i--) {
	    Token tk = new Token(lagrVarsNames[i]);
	    Polinomial p = polinomialMap.get(tk);
	    double[] values = new double[lagrVarsNames.length - i - 1];
	    String[] variables = new String[lagrVarsNames.length - i - 1];
	    for (int j = i + 1; j < lagrVarsNames.length; j++) {
		variables[j - i - 1] = lagrVarsNames[j];
		values[j - i - 1] = resultMap.get(new Token(variables[j - i - 1]));
	    }
	    resultMap.put(tk, p.calc(values, variables));
	}
	double[] result = new double[lagrVarsNames.length];
	for (int i = 0; i < lagrVarsNames.length; i++) {
	    Token tk = new Token(lagrVarsNames[i]);
	    result[i] = resultMap.get(tk);
	}
	return result;
    }

    private final List<Term> terms = new ArrayList<>(5);

    public Polinomial pow(int power) throws ArithmeticException {
	if (power < 0) {
	    throw new ArithmeticException("Unsupported mathematical operation.");
	}
	if (power == 0) {
	    terms.clear();
	    terms.add(new Term().multByElement(new Token(1)));
	    return this;
	}
	Polinomial copy = deepCopy();
	for (int i = 1; i < power; i++) {
	    multByPolinomial(copy);
	}
	return this;
    }

    @SuppressWarnings("AccessingNonPublicFieldOfAnotherObject")
    public Polinomial multByPolinomial(Polinomial p) {
	List<Term> origTerms = new ArrayList<>(terms);
	terms.clear();
	p.terms.stream().forEach((term) -> {
	    origTerms.stream().forEach((oTerm) -> {
		Term newTerm = oTerm.deepCopy();
		newTerm.multByTerm(term);
		addTerm(newTerm);
	    });
	});
	return this;
    }

    public Polinomial multByTerm(Term term) {
	terms.stream().forEach((t) -> t.multByTerm(term));
	return this;
    }

    @SuppressWarnings("AccessingNonPublicFieldOfAnotherObject")
    public Polinomial addPolinomial(Polinomial p) {
	p.terms.forEach((t) -> addTerm(t));
	return this;
    }

    public Polinomial addTerm(Term term) {
	if (terms.isEmpty()) {
	    terms.add(term.deepCopy());
	    return this;
	}
	boolean done = false;
	for (Term t : terms) {
	    if (t.haveSameVars(term)) {
		t.add(term);
		done = true;
		break;
	    }
	}
	if (!done) {
	    terms.add(term.deepCopy());
	}
	return this;
    }

    public Polinomial deepCopy() {
	Polinomial p = new Polinomial();
	terms.stream().forEach((t) -> p.addTerm(t));
	return p;
    }

    @Override
    public String toString() {
	String retVal = terms.stream().map((term) -> term.toString() + " + ").reduce("", String::concat);
	return retVal.substring(0, retVal.length() - 3);
    }

    public String toLaTeX() {
	String retVal = terms.stream().map((term) -> term.toLaTeX() + " + ").reduce("", String::concat);
	return retVal.substring(0, retVal.length() - 3);
    }

    public String toPhi() {
	String retVal = terms.stream().map((term) -> term.toPhi() + ", ").reduce("", String::concat);
	return retVal.substring(0, retVal.length() - 2);
    }

    public String toPhi_i() {
	String retVal = terms.stream().map((term) -> term.toPhi_i() + ", ").reduce("", String::concat);
	return retVal.substring(0, retVal.length() - 2);
    }

    public String calc(String[] varsNames, double[] values) throws IllegalArgumentException {
	if (varsNames.length != values.length) {
	    throw new IllegalArgumentException("Both vectors should have the same length.");
	}
	Map<String, Double> varsValues = new HashMap<>(varsNames.length);
	for (int i = 0; i < varsNames.length; i++) {
	    varsValues.put(varsNames[i], values[i]);
	}
	String result = "";
	double number = 0;
	for (Term t : terms) {
	    StringBuilder temp = new StringBuilder(10);
	    number += t.calc(temp, varsValues);
	    if (!temp.toString().isEmpty()) {
		result += " + " + temp;
	    }
	}
	return number == Math.round(number) ? Math.round(number) + result : number + result;
    }

    public double calc(double[] values, String[] varsNames) throws IllegalArgumentException {
	if (varsNames.length != values.length) {
	    throw new IllegalArgumentException("Both vectors should have the same length.");
	}
	Map<String, Double> varsValues = new HashMap<>(varsNames.length);
	for (int i = 0; i < varsNames.length; i++) {
	    varsValues.put(varsNames[i], values[i]);
	}
	return terms.stream().map((t) -> t.calc(varsValues)).reduce(0.0, Double::sum);
    }

    public Polinomial derive(Token token) throws ArithmeticException {
	if (token.getType() != Token.Type.VARIABLE) {
	    throw new ArithmeticException("Why the fuck are you trying to derivate by a number?");
	}
	Polinomial derivative = new Polinomial();
	terms.stream().filter((term) -> term.hasVar(token)).forEach((term) -> derivative.addTerm(term.derivate(token)));
	return derivative;
    }

    private Polinomial isolate(Token tk) {
	Term tWtk = null;
	for (Term t : terms) {
	    if (t.hasVar(tk)) {
		if (tWtk == null) {
		    tWtk = t;
		} else {
		    throw new IllegalArgumentException("Polinomial have repeated token.");
		}
	    }
	}
	if (tWtk == null) {
	    throw new IllegalArgumentException("Polinomial does not have the token.");
	}
	Polinomial response = new Polinomial();
	double multplier = -1 / tWtk.getNumber();
	terms.stream().filter((t) -> (!t.hasVar(tk)))
		.map((t) -> t.deepCopy()).map((nT) -> nT.multByElement(new Token(multplier))).forEach((nT) -> response.addTerm(nT));
	return response;
    }

    private double getNumberFor(Token tk) {
	for (Term t : terms) {
	    if (t.hasVar(tk)) {
		return t.getNumber();
	    }
	}
	return 0;
    }

    private void removeTermWith(Token tk) {
	terms.removeIf((t) -> t.hasVar(tk));
    }

    public int getNumberOfTerms() {
	return terms.size();
    }

    public void calcPhi(double[] lagrMultValues, XORTableModel inputVectors, double[][] vectorsValues) {
	for (int i = 0; i < vectorsValues.length; i++) {
	    short s = (short) inputVectors.getValueAt(i, inputVectors.getColumnCount() - 1);
	    short[] vals = new short[inputVectors.getColumnCount() - 1];
	    for (int k = 0; k < vals.length; k++) {
		vals[k] = (short) inputVectors.getValueAt(i, k);
	    }
	    for (int j = 0; j < vectorsValues[i].length; j++) {
		Term t = terms.get(j);
		double multp = lagrMultValues[i];
		double phi = t.calcPhi(vals);
		vectorsValues[i][j] = multp * s * phi;
	    }
	}
    }

    public String toPhiVector() {
	String result = "";
	for (int i = 0; i < terms.size(); i++) {
	    if (i > 0) {
		result += " \\\\\n";
	    }
	    result += terms.get(i).toPhi();
	}
	return result;
    }

    public Polinomial reducePhiTo(double[] wOptimal) throws IllegalArgumentException {
	if (wOptimal.length != terms.size()) {
	    throw new IllegalArgumentException("The weight vector must have the same length as the number of terms in the polinomial.");
	}
	Polinomial retVal = new Polinomial();
	for (int i = 0; i < wOptimal.length; i++) {
	    if (Math.abs(wOptimal[i]) > 0.0000001) {
		retVal.addTerm(terms.get(i).getPhi().multByElement(new Token(wOptimal[i])));
	    }
	}
	if (retVal.getNumberOfTerms() == 0) {
	    retVal.addTerm(new Term().multByElement(new Token(0)));
	}
	return retVal;
    }
}
