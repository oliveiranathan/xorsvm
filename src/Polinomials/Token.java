/*            DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
 Version 2, December 2004

 Copyright (C) 2015 Nathan Oliveira <oliveiranathan at gmail.com>

 Everyone is permitted to copy and distribute verbatim or modified
 copies of this code, and changing it is allowed as long  as the
 name is changed.

 DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
 TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION

 0. You just DO WHAT THE FUCK YOU WANT TO.
 */
package Polinomials;

import java.util.Objects;
import org.apache.commons.math3.fraction.Fraction;

/**
 *
 * @author Nathan Oliveira <oliveiranathan at gmail.com>
 */
public class Token { // TODO change to allow fractions and roots...

    private final Type type;
    private final String token;

    private double number;
    private double power;

    public Token(String token) {
	this.type = Type.VARIABLE;
	this.token = token;
	this.number = 0;
	this.power = 1;
    }

    public Token(double number) {
	this.type = Type.NUMBER;
	this.token = null;
	this.number = number;
	this.power = 1;
    }

    public Token(String token, double power) {
	this.type = Type.VARIABLE;
	this.token = token;
	this.number = 0;
	this.power = power;
    }

    public Token(double number, double power) {
	this.type = Type.NUMBER;
	this.token = null;
	this.number = number;
	this.power = power;
    }

    public void incPower() {
	power++;
    }

    public void incPower(double inc) {
	power += inc;
    }

    public Type getType() {
	return type;
    }

    public String getToken() {
	return token;
    }

    public double getNumber() {
	return number;
    }

    public void addNumber(double value) {
	number += value;
    }

    public void multNumber(double value) {
	number *= value;
    }

    public double getPower() {
	return power;
    }

    @SuppressWarnings("AccessingNonPublicFieldOfAnotherObject")
    public boolean isSame(Token tk) {
	if (tk == null) {
	    return false;
	}
	if (type != tk.type) {
	    return false;
	}
	if (type == Type.NUMBER) {
	    return number == tk.number;
	}
	return token.equals(tk.token);
    }

    public Token deepCopy() {
	Token tk;
	if (type == Type.NUMBER) {
	    tk = new Token(number, power);
	} else {
	    tk = new Token(token, power);
	}
	return tk;
    }

    @Override
    public int hashCode() {
	int hash = 5;
	hash = 79 * hash + Objects.hashCode(this.type);
	hash = 79 * hash + Objects.hashCode(this.token);
	hash = 79 * hash + (int) (Double.doubleToLongBits(this.number) ^ (Double.doubleToLongBits(this.number) >>> 32));
	hash = 79 * hash + (int) (Double.doubleToLongBits(this.power) ^ (Double.doubleToLongBits(this.power) >>> 32));
	return hash;
    }

    @Override
    @SuppressWarnings("AccessingNonPublicFieldOfAnotherObject")
    public boolean equals(Object obj) {
	if (obj == null) {
	    return false;
	}
	if (getClass() != obj.getClass()) {
	    return false;
	}
	final Token other = (Token) obj;
	if (this.type != other.type) {
	    return false;
	}
	if (!Objects.equals(this.token, other.token)) {
	    return false;
	}
	if (this.number != other.number) {
	    return false;
	}
	return Double.doubleToLongBits(this.power) == Double.doubleToLongBits(other.power);
    }

    @Override
    public String toString() {
	return type == Type.NUMBER ? "'" + number + "^" + power + "'" : "''" + token + "'^" + power + "'";
    }

    public String toLaTeX() {
	String retVal = "";
	if (type == Type.VARIABLE) {
	    retVal += token;
	} else {
	    Fraction f = new Fraction(number);
	    if (f.getDenominator() == 1) {
		retVal += f.getNumerator();
	    } else {
		retVal += "\\frac{" + f.getNumerator() + "}{" + f.getDenominator() + "}";
	    }
	}
	if (power != 1) {
	    retVal += "^";
	    Fraction f = new Fraction(power);
	    if (f.getDenominator() == 1) {
		retVal += f.getNumerator();
	    } else {
		retVal += "\\frac{" + f.getNumerator() + "}{" + f.getDenominator() + "}";
	    }
	}
	return retVal;
    }

    String toPhi() {
	String retVal = "";
	if (type == Type.VARIABLE) {
	    if (!token.contains("_{i")) {
		retVal += token;
	    } else {
		return retVal;
	    }
	} else {
	    if (number == 1) {
		retVal += Integer.toString((int) number);
	    } else {
		retVal += "\\sqrt{" + Integer.toString((int) number) + "}";
	    }
	}
	if (power != 1) {
	    retVal += "^" + Integer.toString((int) power);
	}
	return retVal;
    }

    String toPhi_i() {
	String retVal = "";
	if (type == Type.VARIABLE) {
	    if (token.contains("_{i")) {
		retVal += token;
	    } else {
		return retVal;
	    }
	} else {
	    if (number == 1) {
		retVal += Integer.toString((int) number);
	    } else {
		retVal += "\\sqrt{" + Integer.toString((int) number) + "}";
	    }
	}
	if (power != 1) {
	    retVal += "^" + Integer.toString((int) power);
	}
	return retVal;
    }

    boolean isPhi() {
	if (type == Type.NUMBER) {
	    return true;
	}
	return !token.contains("_{i");
    }

    @SuppressWarnings("PublicInnerClass")
    public enum Type {

	NUMBER, VARIABLE;
    }
}
